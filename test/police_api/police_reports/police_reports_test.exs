defmodule PoliceApi.PoliceReportsTest do
  use PoliceApi.DataCase

  alias PoliceApi.PoliceReports

  describe "reports" do
    alias PoliceApi.PoliceReports.Report

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def report_fixture(attrs \\ %{}) do
      {:ok, report} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PoliceReports.create_report()

      report
    end

    test "list_reports/0 returns all reports" do
      report = report_fixture()
      assert PoliceReports.list_reports() == [report]
    end
  end

end
