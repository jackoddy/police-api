defmodule PoliceApiWeb.ReportControllerTest do
  use PoliceApiWeb.ConnCase

  alias PoliceApi.PoliceReports
  alias PoliceApi.PoliceReports.Report

  @create_attrs %{
     :crime_id, "test"
     :month, "test"
     :reported_by, "test"
     :falls_within, "test"
     :longitude, "test"
     :latitude, "test"
     :location, "test"
     :lsoa_code, "test"
     :lsoa_name, "test"
     :crime_type, "test"
     :last_outcome_category, "test"
     :context, "test"
  }

  def fixture(:report) do
    {:ok, report} = PoliceReports.create_report(@create_attrs)
    report
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all reports", %{conn: conn} do
      conn = get conn, report_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  test "returns reports sorted by what is specified in params", %{conn: conn} do
    params = %{"sortBy" => "month", "order" => "asc"}
    conn = get conn, report_path(conn, :index, params)
  end

  test "returns reports filtered by what is specified in the params", %{conn: conn} do
    params = %{"filteredBy" => "month", "filteredText" => "asc"}
    conn = get conn, report_path(conn, :index, params)
  end

  test "returns correct page by what is specified in the params", %{conn: conn} do
    params = %{"page" => 10}
    conn = get conn, report_path(conn, :index, params)
  end

  defp create_report(_) do
    report = fixture(:report)
    {:ok, report: report}
  end
end
