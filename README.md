# PoliceAPI

To run this project you will need:
- docker
- a file named `./policeData` in the root of this project containing the downloaded data from `https://data.police.uk/data/`. It is important to select more than one month of data as the seed script expects the csv files to be 2 directories deep (matching pattern `./policeData/*/*`).

example file [here](https://data.police.uk/data/fetch/e60e6b03-b641-48e1-b890-4e3164392620/)

nb: this project expects the csvs to only contain crime data - no outcomes or stop/search

#### Running the Repo

You can set up this project by running:
```
  $ docker-compose up
```
in the root directory of this project.

#### Under the Hood

This will trigger multple steps. A Postgres Database will be created and then migrated by a separate `postgres-migrate` container. Once the database had been migrated the container will exit - triggering the `postgres-seed` container to seed the database from `./PoliceReports`. This triggering is created by the seed container pining the migration container. Once seeded the `police_report_api` container, through the same mechanism is run as well as the frontend container.


#### Using the API
The data can be seen on `localhost:3333`

It is possible to sort this data by clicking the headings - the first click with sort by ascending order and clicking on the same heading again will order by descending.

Filtering the data is also possible using the form at the top of the datatable. Select a corresponding heading and input a string (it does not need to be exact) to filter based on this criteria. e.g. filtering on `crime_type` with `theft` will filter the data to show all the bicycle thefts.

The data is delivered to the front end in paginated form, each page is 30 records and the buttons below the table can be used to navigate through this. It is worth noting that due to time contraints these buttons can be wonky and display minus numbers.

##### a note on testing:
It has not been possible to test this project. Examples are provided in `test/police_api_web/controllers` for what was planned to be tested and how, however time contraints, project scope and the unfamiliarity of the language and framework meant that this was not possible.

##### future improvements:
Besides testing this project, further improvements would prioritise database structure - separating out tables and creating enumerals for common columns, such as `reported_by` and `crime_type`.
