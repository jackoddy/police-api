import React from 'react'
import {Field, reduxForm } from 'redux-form'

const filterForm = ({data, handleSubmit}) => (
    <form onSubmit={handleSubmit}>
        <label for="filterBy">Select Column to filter by</label>
            <Field component='select'  id="filter" name="filterBy">
                {
                    Object.keys(data[0]).map(opt => <option value={opt}>{opt}</option>)
                }
            </Field>
            <label for="filterText">Type filter text here</label>
            <Field  component='input' name="filterText" type="text" />
    </form>
)

export default reduxForm({
  form: 'filter'
})(filterForm)
