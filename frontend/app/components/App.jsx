import React from 'react';
import { connect } from 'react-redux'
import { isEqual } from 'underscore'

import { getReports, updateViewState } from '../actions/reports'

import Pagination from './Pagination'
import DataTable from './DataTable'
import Title from './Title'
import FilterForm from './FilterForm'

class App extends React.Component {

    componentWillMount() {
        this.props.getReports()
    }

    componentWillReceiveProps(nextProps) {
        const newViewState  = nextProps.viewState
        const {viewState, getReports} = this.props

        console.log('new', newViewState)
        console.log('old', viewState)
        if (!isEqual(viewState, newViewState)) getReports(newViewState)
    }

    sort(field) {
        const { updateViewState } = this.props

        updateViewState({
            sortBy: field,
            order: this.sortOrder(field),
            page: 1
        })
    }

    sortOrder(field){
        const { viewState: {sortBy, order} } = this.props

        if (field === sortBy) {
          return  order === 'asc' ? 'desc' : 'asc'
        }

        return 'asc'
    }

  render() {
      const {
          entries,
          getReports,
          viewState: {sortBy, order, page},
          pages,
          updateViewState
      } = this.props

    return (
      <div id="content">
          <div>
              <Title>Police Reports</Title>
              <FilterForm data={entries} onSubmit={updateViewState.bind(this)}/>
              <DataTable onClick={this.sort.bind(this)} sortedBy={sortBy} order={order} data={entries} />
              <Pagination pages={pages} page={page} onClick={updateViewState.bind(this)} />
          </div>
      </div>
    );
  }
}

const mapStateToProps = state =>  ({
    ...state.reports
})

const mapDispatchToProps = {
    getReports,
    updateViewState
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
