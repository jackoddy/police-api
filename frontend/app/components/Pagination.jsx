import React from 'react';

const pageButton = (page, onClick) => (
    <li onClick={() => onClick({page})}>{page}</li>
)

export default ({pages, page, onClick}) => (
    <div>
        <ul>

            {pageButton(page - 10, onClick)}
            {pageButton(page - 1, onClick)}
            <li>Current Page: {page}</li>
            {pageButton(page + 1, onClick)}
            {pageButton(page + 10, onClick)}
        </ul>
    </div>
)
