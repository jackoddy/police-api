import React from 'react'

export default ({data, onClick, sortedBy, order}) => (
    <table>
        <tbody>
            <tr>
                {
                    Object
                        .keys(data[0])
                        .map(key =>(
                            <th className={sortedBy === key ? `active ${order}`: undefined} onClick={() => onClick(key)}>{key}</th>
                        ))

                }
            </tr>
            {data.map(record =>
                <tr>
                    {
                        Object
                            .values(record)
                            .map(value => <td>{value}</td> )
                    }
                </tr>
              )}
        </tbody>
    </table>
)
