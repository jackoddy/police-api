const INIT = {
    entries: [{}],
    viewState: {
        sortedBy: null,
        order: null,
        filteredBy:null,
        filterText: null,
        pages: null,
        page: null,
    }
}

export default (state = INIT, {type, data, viewUpdates}) => {
  switch (type) {
    case 'ADD_REPORTS':
      return {
          ...state,
          entries: data.entries,
      }
  case 'UPDATE_VIEW_STATE':
      return {
          ...state,
          viewState: {
              ...state.viewState,
              ...viewUpdates,
          }
      }
    default:
      return state
  }
};
