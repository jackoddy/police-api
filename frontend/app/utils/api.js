import axios from 'axios'

export default (() => {
    const client = () => axios.create({
    })

  return {
      getReports: params => client()
          .get('http://localhost:4000', {
              params
          })
  }
})()
