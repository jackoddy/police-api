import api from '../utils/api'

export const getReports = (params={}) => dispatch => {
    api
        .getReports(params)
        .then(({data}) => dispatch({type: 'ADD_REPORTS', data}))
}

export const updateViewState = viewUpdates => ({
    type: 'UPDATE_VIEW_STATE',
    viewUpdates
})
