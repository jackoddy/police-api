defmodule PoliceApiWeb.ReportView do
  use PoliceApiWeb, :view
  alias PoliceApiWeb.ReportView

  def render("index.json", %{reports: reports}) do
    reports
  end
end
