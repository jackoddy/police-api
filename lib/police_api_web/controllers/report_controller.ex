defmodule PoliceApiWeb.ReportController do
  use PoliceApiWeb, :controller

  alias PoliceApi.PoliceReports
  alias PoliceApi.PoliceReports.Report

  action_fallback PoliceApiWeb.FallbackController

  def index(conn, params) do
    reports = PoliceReports.list_reports(params)
    render(conn, "index.json", reports: reports)
  end

end
