defmodule PoliceApiWeb.Router do
  use PoliceApiWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: ~r/[\s\S]+/
    plug :accepts, ["json"]
  end

  scope "/", PoliceApiWeb do
    pipe_through :api

    get "/", ReportController, :index
  end
end
