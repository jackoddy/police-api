defmodule PoliceApi.PoliceReports do
  @moduledoc """
  The PoliceReports context.
  """

  import Ecto.Query, warn: false
  alias PoliceApi.Repo

  alias PoliceApi.PoliceReports.Report

  @doc """
  Returns the list of reports.

  ## Examples

      iex> list_reports()
      [%Report{}, ...]

  """
  def list_reports(params) do
      Report
      |> maybe_filter(params)
      |> maybe_sort(params)
      |> Repo.paginate(params)
  end

  def maybe_sort(query, %{"sortBy" => sortBy, "order" => "asc"}) do
    field = String.to_atom(sortBy)
    query
      |> order_by([asc: ^field])
  end
  def maybe_sort(query, %{"sortBy" => sortBy, "order" => "desc"}) do
    field = String.to_atom(sortBy)
    query
    |> order_by([desc: ^field])
  end
  def maybe_sort(query, _), do: query

  def maybe_filter(query, %{"filterBy" => filterBy, "filterText" => filterText}) do
    column = String.to_atom(filterBy)

    from r in Report,
      where: ilike(field(r, ^column), ^("%#{filterText}%"))
  end
  def maybe_filter(query, _), do: query

end
