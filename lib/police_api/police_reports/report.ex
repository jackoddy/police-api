defmodule PoliceApi.PoliceReports.Report do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, except: [:__meta__] }

  schema "police_report" do
    field :crime_id, :string
    field :month, :string
    field :reported_by, :string
    field :falls_within, :string
    field :longitude, :string
    field :latitude, :string
    field :location, :string
    field :lsoa_code, :string
    field :lsoa_name, :string
    field :crime_type, :string
    field :last_outcome_category, :string
    field :context, :string
  end

  @doc false
  # def changeset(report, attrs) do
  #   report
  #   |> cast(attrs, [:name])
  #   |> validate_required([:name])
  # end
end
