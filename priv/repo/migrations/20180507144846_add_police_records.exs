defmodule PoliceApi.Repo.Migrations.AddPoliceRecords do
  use Ecto.Migration

  def up do
    create table(:police_report) do
      add :crime_id, :string, primary: true, length: 64
      add :month, :string, length: 7
      add :reported_by, :string
      add :falls_within, :string
      add :longitude, :string
      add :latitude, :string
      add :location, :string
      add :lsoa_code, :string
      add :lsoa_name, :string
      add :crime_type, :string
      add :last_outcome_category, :string
      add :context, :string
    end
  end
  def down do
    drop table(:police_report)
  end
end
