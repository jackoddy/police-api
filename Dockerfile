from bitwalker/alpine-elixir-phoenix

workdir /usr/src/app

copy ./ ./

run mix do deps.get, deps.compile

cmd mix server
